# Scripts
All scripts are written for bash
 
<br>

## createClientCertificate.sh
Generating a single client certificate.

Usage: ./createClientCertificate.sh smartstreetlamp

# 

## createDTCCertificate.sh
Generating a DCU certtificate

Usage: ./createDCUCertificate.sh
# 
## renewDTC
Renew an existing DCU certificate

Usage: ./createDTCCertificate.sh
# 
## createRootCertificate.sh
Generate a new CA. If the parameter "-n" is appended, the previous CA will be deleted.

Usage: ./createRootCertificate.sh
# 
## clean.sh
A helper to clean the CA. Is automatically executed when a new CA should be generated.
the "-a" option cleans the CA directory

Usage: ./clean.sh -a
# 
## genkeypair
Generate an ECC key pair and a key parameter file, if not already present.

Usage: ./genkeypair *outfile*
# 
# Config

## ca.cnf

Config for for all CA-related files and for signing certificates.
# 
## client.cnf

Contains extensions for client certificates
# 

## server.cnf

Contains server extensions for certificates