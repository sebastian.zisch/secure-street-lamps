#!/bin/bash

if [ $(id -u) != 0 ]; then
	echo "I want root permissions"
	exit 0
fi

LETTERBOX_DIR="../Letterbox"

# Text placeholder for making another config per client
# e-mail-address 	tbd-mail
# common name 		tbd-cn
# input password	tbd-inpass
# output password 	tbd-outpass

# Replace a given text in a file for the new
ReplaceTBD() {
	# $1: tbd-clause
	# $2: replacement text
	# $3: file
	
	# Search for tbd clause and replace it with the new
	# Syntax: s/<regex>/<replacement>/
	RES=$(sed "s/$1/$2/" $3)
	
	
	if [ -z "$RES" ]
	then
		# sed couldn't find the specified text and returned a null string
		echo "Replacement text \" $2 \" could not be found in $3"
	else
		#Overwrite File with new text
		echo "$RES" > $3
	fi
}

CLIENT_PW="-"
PW_ENCRYPT="-nodes"
CA_NAME=$(grep '= "'  ca.cnf | sed 's/.*=//;s/^ *//;s/"//g')
PASSWORD_CA=$(grep input_password ca.cnf | sed 's/.*=//')


if [ -z $CLIENT_PW ]
then
	echo "Password not supplied. Generating..."
	CLIENT_PW=$(openssl rand -base64 16 | sed 's/=.*//')
	while [ ! -z $(grep "/" <<< $CLIENT_PW) ]
	do
		CLIENT_PW=$(openssl rand -base64 16 | sed 's/=.*//')
	done
fi

#CONFIGRUNS=$(ls -l |grep -c server.c.*)
SERVER_CNF="server.cnf"

#ReplaceTBD tbd_inpass $CLIENT_PW $SERVER_CNF
#ReplaceTBD tbd_outpass $CLIENT_PW $SERVER_CNF

#First create client certificate sign request (csr)
# -new : create new client ceritifcate
# -newkey : define new private key for client with rsa 2048 bits
# $PW_ENCRYPT: will be -nodes if key should not be encrypted



#Generate keypair
if [ ! -e genkeypair ]; then
	echo "Cannot find genkey script"
	exit 0;
else
	echo "Generating new Keypair"
	./genkeypair "server.key"
fi
openssl req -new -key server.key -keyform PEM \
	-out server.csr -config $SERVER_CNF

#Sign the server certificate

echo "signing the certificate"

# openssl ca : CA application to sign requests
# -batch 	: no questions asked, just sign
# -keyfile	: Private key of the CA
# -cert		: Certificate of the CA
# -in		: Certificate Sign Request (csr) to be signed by the CA
# -key		: Password of the CA's private key (File should only be readable by the root user)

openssl ca -batch -in server.csr \
	-days 1 -extfile server.cnf -extensions v3_server \
	-out servercert.pem -config ca.cnf

#openssl x509 -req -in server.csr \
#	-extfile server.cnf -extensions v3_server \
#	-CA cacert.pem -CAkey ca_key.pem\
#	-days 1 -CAcreateserial \
#	-out servercert.pem

#Readable for everyone
chmod a+r servercert.pem

#Move to letterbox for automated retrieval
cp servercert.pem $LETTERBOX_DIR
