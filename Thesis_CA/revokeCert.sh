#!/bin/bash

if [ $(id -u) != 0 ]; then
	echo "I want root permissions"
	exit 0
fi

LETTERBOX_DIR = "../Letterbox"

REASON="cessationOfOperation"

if [ -z "$(grep CERTIFICATE $1)" ]; then
	echo "$1 : Not a certificate file"
	
fi

if [ -z $2 ]; then
	echo "Reason is default : $REASON"
else
	echo "newReason: $2"
	REASON=$2
fi

openssl ca -revoke $1 -crl_reason $REASON -config ca.cnf &&
openssl ca -gencrl -updatedb -crlexts crl_ext -out crl.pem -config ca.cnf &&
openssl crl -text -noout -in crl.pem &&
echo "successfully revoked the certificate"

#Move to letterbox for automated retrieval
cp crl.pem $LETTERBOX_DIR
