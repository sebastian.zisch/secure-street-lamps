#!/bin/bash

if [ $(id -u) != 0 ]; then
	echo "I want root permissions"
	exit 0
fi

#Print a usage function of how to use this script

Help() {
	echo "This is the help function of this script"
	echo "It will create a client script using a certification authority ca.pem in the script-directory"
	echo "Syntax: createCLientCertificate [-h ] [-p password] clientname"
	echo 
	echo "options:"
	echo "-p password 	password for encrypting the keyfile. Generate one if not specified and save it to PW"
	echo "-c config		Configuration file for the client. Defaults to client.cnf in the script-directory"
	echo "-e 			Flag to enable encryption of password. Disabled by default"
	echo "-h 			Print this help dialog"
	#echo "-n nr			create nr of client certificates with a std name"
	echo
}

# Text placeholder for making another config per client
# e-mail-address 	tbd-mail
# common name 		tbd-cn
# input password	tbd-inpass
# output password 	tbd-outpass

# Replace a given text in a file for the new
ReplaceTBD() {
	# $1: tbd-clause
	# $2: replacement text
	# $3: file
	
	# Search for tbd clause and replace it with the new
	# Syntax: s/<regex>/<replacement>/
	
	RES=$(sed "s|$1|$2|" $3)
	
	
	if [ -z "$RES" ]
	then
		# sed couldn't find the specified text and returned a null string
		echo "Replacement text \" $1 \" could not be found in $3"
	else
		#Overwrite File with new text
		echo "$RES" > $3
	fi
}

CA_NAME=$(grep '= "'  ca.cnf | sed 's/.*=//;s/^ *//;s/"//g')
CRT_ENDDATE=99991231235959Z

#Loop through all parameters
while [ -n $1 ]
do	
	case $1 in
		-h) Help
		   exit 0 ;;
#Create password with openssl in base64 encoding and remove all trailing equal signs
		-p) 	
			CLIENT_PW=$2
			#Remove second parameter
			shift ;;
		-c) 	
			CLIENT_CNF=$2
			shift ;;
			
		-e)	
			PW_ENCRYPT="-noenc"
			;;
		-t) 	
			CRT_ENDDATE="-enddate $2"
			shift
			;;
			
		*) 	
			break ;;
			
	esac
	shift
done

CLIENT_CN=$1
echo "Client name: $CLIENT_CN"
#Read client Name
if [ -z $CLIENT_CN ]
then
	echo "No client name supplied"
	Help
	exit 0
fi

#Prepare new Directory for client certificates
if [ ! -d $CLIENT_CN ] 
then
	mkdir $CLIENT_CN
else
	echo "Client directory is present. Renewing certificate"
	
fi

#Check if certificate file is present
if [ ! -e "$CA_NAME.pem" ] || [ ! -e .ca_key.pem ] || [ ! -e ca.cnf ]
then 
	echo "Certification instance file could not be found. Please create a config, key and certificate"
	exit 0
fi

CA_NAME=$(grep '= "'  ca.cnf | sed "s/.*=//;s/^ *//")
PASSWORD_CA=$(grep input_password ca.cnf | sed 's/.*=//')


if [ ! -f "$CLIENT_CN/$CLIENT_CNF.cnf" ]
then
	echo "Client config not found."
	echo "Using default config as a template"
	#Copy config to new file in the client's directory
		
	cp -f client.cnf "./$CLIENT_CN/$CLIENT_CN.cnf"
	CLIENT_CNF="$CLIENT_CN/$CLIENT_CN.cnf"
	
	#Replace Common name
	ReplaceTBD tbd-cn $CLIENT_CN $CLIENT_CNF
	#Replace email
	ReplaceTBD tbd-mail $CLIENT_CN $CLIENT_CNF
else
	echo "Found the configfile of this client"
	CLIENT_PW=$(grep input_password "$CLIENT_CN/$CLIENT_CN.cnf" | sed 's/.*= //')
	echo "clientpw: $CLIENT_PW"
fi

if [ -z $CLIENT_PW ]
then
	echo "Password not supplied. Generating..."
	#Remove all tailing equal signs
	ERROR=""
	CLIENT_PW=$(openssl rand -base64 16 | sed 's/=.*//')
	while [ ! -z $(grep "/" <<< $CLIENT_PW) ]
	do
		CLIENT_PW=$(openssl rand -base64 16 | sed 's/=.*//')
	done
fi

#Replace password in client config
ReplaceTBD tbd_inpass "$CLIENT_PW" $CLIENT_CNF
ReplaceTBD tbd_outpass "$CLIENT_PW" $CLIENT_CNF

#Generate keypair
if [ ! -e genkeypair ]; then
	echo "Cannot find genkey script"
	exit 0;
else
	echo "Generating Keypair"
	./genkeypair "client.key"
fi

#First create client certificate sign request (csr)
# -new : create new client ceritifcate
# -newkey : define new private key for client with rsa 2048 bits
# $PW_ENCRYPT: will be -nodes if key should not be encrypted
openssl req -new -key client.key -keyform PEM \
	-out $CLIENT_CN/$CLIENT_CN.csr -config $CLIENT_CNF

chmod go+r client.key
chmod go+r $CLIENT_CNF

mv client.key "$CLIENT_CN/$CLIENT_CN.key"
#Sign the client certificate

echo "signing the certificate"

# openssl ca : CA application to sign requests
# -batch 	: no questions asked, just sign
# -keyfile	: Private key of the CA
# -cert		: Certificate of the CA
# -in		: Certificate Sign Request (csr) to be signed by the CA
# -key		: Password of the CA's private key (File should only be readable by the root user)

openssl ca -batch -in $CLIENT_CN/$CLIENT_CN.csr \
	-out "$CLIENT_CN/$CLIENT_CN.pem" \
	-enddate $CRT_ENDDATE \
	-extfile client.cnf -extensions v3_client \
	-config ca.cnf

#openssl x509 -req -in $CLIENT_CN/$CLIENT_CN.csr \
#	-extfile client.cnf -extensions v3_client \
#	-CA cacert.pem -CAkey ca_key.key \
#	-out $CLIENT_CN/$CLIENT_CN.pem

rm $CLIENT_CN/$CLIENT_CN.csr

mv -f $CLIENT_CN clients


