#!/bin/bash

if [ ! -x $0 ]
then
	echo "please run as root"
	exit 0
fi

Help(){
	echo "Cleaning script. Usage:"
	echo "Clean one client"
	echo "./clean.sh -c <dir-of-cert>"
	echo "Clean everything. This includes CA, serial numbers, index database etc"
	echo "./clean.sh -a"
}

#Removes already certified certificates
CleanClientCerts(){
	for i in $( ls "certificates" ); do
		if [ ! -z "$(grep $1 "certificates/$i")" ]
		then
			echo "Removing $i"
			rm "certificates/$i"
		fi
	done
	grep -v "$1" index.db > tmpfile && mv tmpfile index.db
}

CA_NAME=$(grep '= "'  ca.cnf | sed "s/.*=//;s/^ *//")

#Loop through all parameters
while [ -n $1 ]
do
	case $1 in
#Create password with openssl in base64 encoding and remove all trailing equal signs
		-c) 	basename $2
			echo "CLEANING $2"
			CleanClientCerts $2
			rm -r $2
			shift ;;
			
		-a)	echo "Removing everything"
			rm certificates/*
			rm -r clients/*
			rm ca.csr .ca_key.pem .certs.db* .serial_number* .crl_number*
			rm ../Letterbox/*.pem
			rm SecureLamps-Root-CA.pem
			rm servercert.pem server.key server.csr
			rm crl.pem
			echo "" > .lastrequest
			break ;;
		*) 	Help
			exit 0;
	shift ;;
	esac
done

echo "Done Cleaning"
