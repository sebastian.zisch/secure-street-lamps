#!/bin/bash

if [ $(id -u) != 0 ]; then
	echo "I want root permissions"
	exit 0
fi

LETTERBOX_DIR = "../Letterbox"

# Text placeholder for making another config per client
# e-mail-address 	tbd-mail
# common name 		tbd-cn
# input password	tbd-inpass
# output password 	tbd-outpass

# Replace a given text in a file for the new
ReplaceTBD() {
	# $1: tbd-clause
	# $2: replacement text
	# $3: file
	
	# Search for tbd clause and replace it with the new
	# Syntax: s/<regex>/<replacement>/
	RES=$(sed "s/$1/$2/" $3)
	
	
	if [ -z "$RES" ]
	then
		# sed couldn't find the specified text and returned a null string
		echo "Replacement text \" $2 \" could not be found in $3"
	else
		#Overwrite File with new text
		echo "$RES" > $3
	fi
}

Help() {
	echo -e "Command Error. \n Syntax: ./renewDTC.sh <csr-filename>"
}

CA_NAME=$(grep '= "'  ca.cnf | sed "s/.*=//;s/^ *//")
#Get CA Password
CA_PASSWORD=$(grep output_password ca.cnf | sed 's/.*=//;s/^ *//')
SIGNREQ="server.csr"

if [ -z $SIGNREQ ]; then
	Help
	exit 0
fi

if [ ! -e $SIGNREQ ]; then
	echo "$SIGNREQ was not found"
	exit 0
fi

#Check for time of last request
if [ ! -e .lastrequest ]; then
	echo "No request made yet."
	touch .lastrequest
	date '+%s' > .lastrequest
else
	PREV=$(cat lastrequest)
	CURR=$(date '+%s')
	DIFF=$((CURR-PREV))
	
	if [ $DIFF -lt 10  ]; then
		echo "not so many requests at once"
		exit 0;
	fi
	echo "$CURR" > .lastrequest
fi

#umask 077

#Check if stored private key matches the csr public key - not really necessary, as the thief doesn't need to know private key (except with TPM)
#Get public key from sign request
#PUBKEY_REQUEST=$(openssl req -pubkey -noout -in $SIGNREQ)
#Calculate private key from private key file
#PRIVKEY_SERVER=$(openssl rsa -pubout -in server_privkey.pem)

#if [[ $PUBKEY_REQUEST != $PRIVKEY_SERVER ]]; then
#	echo "Requester is not the DTC. Public and private key don't match"
#	exit 0;
#fi

#cp $SIGNREQ $OLDCSRS_SERVER/$SIGNREQ
#Sign the certificate, as requester has the correct private key

#Revoking the old certificate
echo "Revoking the old certificate"
bash revokeCert.sh servercert.pem

echo "Refreshing the certificate for 24 hours from now"

# openssl ca : CA application to sign requests
# -batch 	: no questions asked, just sign
# -keyfile	: Private key of the CA
# -cert		: Certificate of the CA
# -in		: Certificate Sign Request (csr) to be signed by the CA
# -key		: Password of the CA's private key (File should only be readable by the root user)
openssl ca -batch -in $SIGNREQ \
	-days 1 -out servercert.pem \
	-extfile server.cnf -extensions v3_server \
	-config ca.cnf 2>errfile

if [ -z "$(cat errfile | grep ERROR)" ]; then
	rm errfile
else
	echo "An error occurred, please look in errfile"
	exit 0;
fi

#Move to letterbox for automated retrieval
cp servercert.pem $LETTERBOX_DIR
