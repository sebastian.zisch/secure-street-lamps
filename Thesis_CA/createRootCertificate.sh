#!/bin/bash

if [ $(id -u) != 0 ]; then
	echo "I want root permissions"
	exit 0
fi

LETTERBOX_DIR="../Letterbox"

if [ ! -f ca.cnf ]; then
	echo "No config file found"
	exit 0;
fi

if [[ $1 == "-n" ]]; then
	CREATE_NEW=1
fi

#Read passwords from config files
#Copied from freeradius makefile
CA_NAME=$(grep '= "'  ca.cnf | sed 's/.*=//;s/^ *//;s/"//g')
CA_KEYFILE=.ca_key.pem
CA_PASSWORD=$(grep output_password ca.cnf | sed 's/.*=//;s/^ *//')
CA_DEFAULT_DAYS=$(grep default_days ca.cnf | sed 's/.*=//;s/^ *//')

CRT_ENDDATE=99991231235959Z

# Only root write permissions
#umask u=rwx,go=

if [ -e $CA_KEYFILE ];
then
	if [ $CREATE_NEW == 0 ]; then
		echo "Private key file exists aka a CA is already in place.
			Please use -n option to create a new CA. Use with caution!"
			exit 0;
	else
		echo "Chose to create a new CA."
		echo "CA private key file exists. removing it"
		echo "call clean script"
		if [ ! -e clean.sh ]; then
			echo "No cleaning script found. This might cause problems"
		else
			bash clean.sh -a
		fi
		rm $CA_KEYFILE
	fi
fi

echo "Creating a self-signed (root) certificate structure for the backend"

touch .certs.db
echo "01" > .crl_number
#Create keypair

#Parameter description:
# -new : create a new request
# -keyout: location of new key
# -newkey: algorithm and bitnumber for the new key
# -out 	: certificate sign request for the ca
# -config : use ca.cnf

#Generate keypair
if [ ! -e genkeypair ]; then
	echo "ERROR: Cannot find genkey script"
	exit 0;
else
	echo "Generating Keypair"
	./genkeypair "$CA_KEYFILE"
fi

openssl req -new -key $CA_KEYFILE -keyform PEM -out ca.csr -config ./ca.cnf

#Sign this certificate and create a serial file
openssl ca -batch -in ca.csr -keyfile $CA_KEYFILE \
	-create_serial -out "$CA_NAME.pem" -selfsign\
	-enddate $CRT_ENDDATE \
	-extfile ca.cnf -extensions v3_ca \
	-config ca.cnf

#openssl x509 -req -in ca.csr \
#	-extfile ca.cnf -extensions v3_ca \
#	-key .ca_key.pem -days $CA_DEFAULT_DAYS \
#	-out "$CA_NAME.pem"

#rm ca.csr

echo "Creating Certificate Revocation List"
#Create a certificate Revocation List
openssl ca -gencrl -crlexts crl_ext -out crl.pem -config ca.cnf

chmod a+r crl.pem
chmod a+r "$CA_NAME.pem"

#move to letterbox for automated retrieval
cp crl.pem $LETTERBOX_DIR
cp "$CA_NAME.pem" $LETTERBOX_DIR
