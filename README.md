# Secure Street Lamps

This repository contains an implementation for a security architecture for intelligent street lamps.

## Hostapd
Contains a sample configuration

## Thesis_CA

Contains the files for a certification authority

## pam_mount_config

Contains the pam-mount config for mounting the encrypted home directory

## renewCert

A shell script to update keys and certificates over the network