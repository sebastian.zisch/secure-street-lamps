import argparse
import logging
import os

from pyftpdlib.handlers import FTPHandler
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.servers import FTPServer
class Handler(FTPHandler):
	def on_file_sent(self, file):
		logging.info("send file! {}".format(file))
		
	def on_file_received(self, file):
		logging.info("received file! {}".format(file))
		

logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument('--user', default="ftpuser", help='ftp user name. default is user')
parser.add_argument('--password', default="ftppass", help='ftp password default is password')
parser.add_argument('--port', default=2121, help='default port is 21')
args = parser.parse_args()


authorizer = DummyAuthorizer()
authorizer.add_user('ftpuser', 'ftppass', ".", perm='elradfmwMT')
authorizer.add_anonymous(os.getcwd())

handler=FTPHandler
handler.authorizer = authorizer

server = FTPServer(('', 2121), handler)
server.serve_forever()
