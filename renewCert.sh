#!/bin/bash

echo "Renewing certificates"

if [ ! -e ".ssh/id_ecdsa" ]; then
	echo "Could not find identitiy file for server-refresh (.ssh/id_ecdsa)."
	echo "Please create respective identity-file."
	exit 0;
elif [ ! -e ".ssh/id_ecdsa_crl" ]; then
	echo "Could not find identitiy file for CRL-refresh (.ssh/id_ecdsa_crl)."
	echo "Please create respective identity-file."
	exit 0;
else
	ssh -i .ssh/id_ecdsa_crl sebastian@kaliseb.local > cacrltmp
	ssh -i .ssh/id_ecdsa sebastian@kaliseb.local > servertmp
fi

if [ -e servercert.pem ] ; then
	CURR=$(openssl x509 -in servercert.pem -noout -serial)
	NEW=$(openssl x509 -in servertmp -noout -serial)
	if [[ $CURR == $NEW ]] ; then
		echo "You already have the newest certificate. Not renewed"
	else
		mv servertmp AP/servercert.pem
		echo "Server cerificate updated"
	fi
	echo "$CURR, $NEW"

else 
	mv servertmp AP/servercert.pem
fi
