if [ -e SecureLamps-Root-CA.pem ] && [ -e crl.pem ]; then
    cat SecureLamps-Root-CA.pem crl.pem
else
    echo ""
fi
