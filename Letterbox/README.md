# Letterbox contents

## getCRL.sh
 A simple one-line script for concatenating the root-certificate and the CRL
 <br />

 Intended for SSH-Forced commands
# 

## getDCUCert.sh
 Just prints the DCU certificate to standard output
 <br />

 Intended for SSH-Forced commands